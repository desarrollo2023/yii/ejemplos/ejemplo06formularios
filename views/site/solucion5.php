<?php

echo \yii\widgets\DetailView::widget([
    "model" => $model,
    "attributes" => [
        "nombre",
        "apellidos",
        "fechaNacimiento",
        "correo",
        "poblacion",
        "mesesTexto"
    ],
]);
