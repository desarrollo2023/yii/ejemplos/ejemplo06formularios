<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\formulario2 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio2">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numero1') ?>
    <?= $form->field($model, 'numero2') ?>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio2 -->