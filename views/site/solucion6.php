<?php

echo \yii\widgets\DetailView::widget([
    "model" => $model, "attributes" => [
        "activado", "mes", "opcion", "dia", "id", "fecha", [
            "attribute" => "nombre",
            "format" => "raw",
            "value" => function ($model) {
                return \yii\helpers\Html::a(
                    $model->nombre,
                    "@web/pdf/" . $model->nombre,
                    ["class" => "btn btn-primary"]
                );
            }
        ]
    ],
]);
