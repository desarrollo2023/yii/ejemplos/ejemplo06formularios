<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    "model" => $model,
    "attributes" => [
        "numero1", "numero2",
        [
            "attribute" => "Suma", "value" => function ($model) {
                return $model->suma();
            }
        ],
    ]
]);

?>

<div style="font-size: medium;font-weight:bold;border:1px solid #ccc;padding:10px">
    <div>Suma: <?= $model->suma() ?> </div>
    <div>Resta: <?= $model->resta() ?></div>
    <div>Producto: <?= $model->producto() ?> </div>
    <div>Cociente: <?= $model->cociente() ?></div>
</div>