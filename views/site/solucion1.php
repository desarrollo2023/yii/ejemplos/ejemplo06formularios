<?php

use yii\widgets\DetailView;

echo DetailView::widget(["model" => $model]);

?>

<h5>La suma es: <?= $model->suma() ?></h5>
<h5>El número de vocales es: <?= $model->vocales1() ?></h5>