<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\formulario3 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio3">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dia')->input('number', ['placeholder' => 'Escribe número de día']) ?>
    <?= $form->field($model, 'mes')->dropDownList($model->meses(), ['prompt' => 'Selecciona un mes']) ?>
    <?= $form->field($model, 'poblacion')->listBox($model->poblaciones()) ?>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio3 -->