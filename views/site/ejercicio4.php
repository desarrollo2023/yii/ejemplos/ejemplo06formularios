<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\formulario4 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio4">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(["placeholder" => "Introduce nombre completo"]) ?>
    <?= $form->field($model, 'edad')->input("number") ?>
    <?= $form->field($model, 'mes')->dropDownList($model->meses, ["prompt" => "Selecciona el mes"]) ?>
    <?= $form->field($model, 'categoria')->radioList($model->categorias) ?>
    <?= $form->field($model, 'dia')->listBox($model->dias) ?>
    <?= $form->field($model, 'aficiones')->checkboxList($model->mostraraficiones) ?>
    <?= $form->field($model, 'opciones')->listBox($model->mostraropciones, ["multiple" => "multiple"]) ?>
    <?= $form->field($model, 'fecha')->input("date") ?>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio4 -->