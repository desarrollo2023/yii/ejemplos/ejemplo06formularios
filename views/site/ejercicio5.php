<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\formulario5 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio5">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(["placeholder" => "Introduce tu nombre"]) ?>
    <?= $form->field($model, 'apellidos')->textInput(["placeholder" => "Introduce tus apellidos"]) ?>
    <?= $form->field($model, 'fechaNacimiento')->input("date") ?>
    <?= $form->field($model, 'correo')->input("email") ?>
    <?= $form->field($model, 'poblacion')->dropDownList($model->poblaciones, ["prompt" => "Escoge una población"]) ?>
    <?= $form->field($model, 'mes')->checkboxList($model->meses) ?>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio5 -->