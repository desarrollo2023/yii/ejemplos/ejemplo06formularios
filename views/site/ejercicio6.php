<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\formulario6 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio6">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'activado')->checkbox($model->activar())  ?>
    <?= $form->field($model, 'mes')->dropDownList($model->meses(), ["prompt" => "Selecciona mes"]) ?>
    <?= $form->field($model, 'opcion')->radioList($model->opciones()) ?>
    <?= $form->field($model, 'dia')->listBox($model->dias()) ?>
    <?= $form->field($model, 'id')->input("number") ?>
    <?= $form->field($model, 'fecha')->input("date") ?>
    <?= $form->field($model, 'nombre')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio6 -->