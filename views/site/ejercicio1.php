<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\formulario1 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio1">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numero1')->input("number") ?>
    <?= $form->field($model, 'numero2')->input("number") ?>
    <?= $form->field($model, 'descripcion') ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio1 -->