<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionEjercicio1()
    {
        $model = new \app\models\formulario1();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return $this->render("solucion1", ["model" => $model]);
            }
        }

        return $this->render('ejercicio1', [
            'model' => $model,
        ]);
    }
    public function actionEjercicio2()
    {
        $model = new \app\models\formulario2();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return $this->render("solucion2", ["model" => $model]);
            }
        }

        return $this->render('ejercicio2', [
            'model' => $model,
        ]);
    }
    public function actionEjercicio3()
    {
        $model = new \app\models\formulario3();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render("solucion3", [
                    "model" => $model
                ]);
            }
        }

        return $this->render('ejercicio3', [
            'model' => $model,
        ]);
    }
    public function actionEjercicio4()
    {
        $model = new \app\models\formulario4();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return $this->render("solucion4", [
                    "model" => $model
                ]);
            }
        }

        return $this->render('ejercicio4', [
            'model' => $model,
        ]);
    }
    public function actionEjercicio5()
    {
        $model = new \app\models\formulario5();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return $this->render("solucion5", ["model" => $model]);
            }
        }

        return $this->render('ejercicio5', [
            'model' => $model,
        ]);
    }
    public function actionEjercicio6()
    {
        $model = new \app\models\formulario6();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return $this->render("solucion6", ["model" => $model]);
            }
        }

        return $this->render('ejercicio6', [
            'model' => $model,
        ]);
    }
}
