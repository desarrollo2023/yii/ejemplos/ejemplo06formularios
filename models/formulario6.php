<?php

namespace app\models;

use DateTime;
use yii\base\Model;

/**
 * Formulario6
 *  */
class Formulario6 extends Model
{
    public ?string $activado = null;
    public ?string $mes = null;
    public ?string $opcion = null;
    public ?string $dia = null;
    public ?int $id = 0;
    public ?string $fecha = null;
    public $nombre;

    public function rules(): array
    {
        return [
            [["activado", "mes", "opcion", "dia", "id", "fecha"], "safe"],
            [['nombre'], 'file', 'skipOnEmpty' => false, 'extensions' => 'pdf'],

        ];
    }
    public function attributeLabels(): array
    {
        return [
            "opcion" => "Opciones a elegir",
            "dia" => "Selecciona uno o varios días",
            "id" => "Id",
            "fecha" => "Fecha de reserva",
            "nombre" => "Examinar"
        ];
    }


    public function subirArchivo(): bool
    {
        $this->nombre->saveAs('pdf/' . $this->nombre->name);
        return true;
    }


    /**
     * antes de validar cojo los archivos enviados y
     * los coloco en el modelo
     * 
     */
    public function beforeValidate()
    {
        $this->nombre = \yii\web\UploadedFile::getInstance($this, 'nombre');
        return true;
    }

    /**
     * despues de validar subo el archivo
     * 
     */
    public function afterValidate()
    {
        $this->subirArchivo();
        return true;
    }
    public function meses(): array
    {
        return [
            "Enero" => "Enero",
            "Febrero" => "Febrero",
            "Marzo" => "Marzo",
            "Abril" => "Abril",
            "Mayo" => "Mayo",
            "Junio" => "Junio",
            "Julio" => "Julio",
            "Agosto" => "Agosto",
            "Septiembre" => "Septiembre",
            "Octubre" => "Octubre",
            "Noviembre" => "Noviembre",
            "Diciembre" => "Diciembre",
        ];
    }
    public function opciones(): array
    {
        return ["Opción1" => "Opción1", "Opción2" => "Opción2", "Opción3" => "Opción3"];
    }
    public function dias(): array
    {
        return ["Lunes" => "Lunes", "Martes" => "Martes", "Miércoles" => "Miércoles", "Jueves" => "Jueves", "Viernes" => "Viernes"];
    }
}
