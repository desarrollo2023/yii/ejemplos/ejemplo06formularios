<?php

namespace app\models;

use yii\base\Model;

/**
 * Formulario1
 */
class Formulario1 extends Model
{
    public ?int $numero1 = null; // input tipo numero
    public ?int $numero2 = null; // input tipo numero
    public ?string $descripcion = null; // input tipo texto

    public function attributeLabels(): array
    {
        return [
            "numero1" => "Número 1",
            "numero2" => "Número 2",
            "descripcion" => "Descripcion",
        ];
    }

    public function rules()
    {
        return [
            [["numero1", "numero2", "descripcion"], "required"]
        ];
    }
    public function suma()
    {
        return $this->numero1 + $this->numero2;
    }
    public function vocales()
    {
        $caracter = str_split(strtolower($this->descripcion));
        $contador = 0;
        foreach ($caracter as $letra) {
            if ($letra == "a" || $letra == "e" || $letra == "i" || $letra == "o" || $letra == "u") {
                $contador++;
            }
        }
        return $contador;
    }
    public function vocales1()
    {
        $caracter = str_split(strtolower($this->descripcion));
        $contador = 0;
        foreach ($caracter as $letra) {
            if (in_array($letra, ['a', 'e', 'i', 'o', 'u'])) {
                $contador++;
            }
        }
        return $contador;
    }
}
