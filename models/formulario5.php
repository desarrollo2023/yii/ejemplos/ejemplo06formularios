<?php

namespace app\models;

use DateTime;
use yii\base\Model;

/**
 * Formulario5
 *  */
class Formulario5 extends Model
{
    public ?string $nombre = null; //cuadro de texto
    public ?string $apellidos = null; //cuadro de texto  
    public ?string $fechaNacimiento = null; // fecha  ant 31/12/23
    public ?string $correo = null; //correo
    public ?string $poblacion = null; //dropdownlist
    public array $mes = []; //checkbox


    public function attributeLabels(): array
    {
        return [
            "nombre" => "Nombre",
            "apellidos" => "Apellidos",
            "fechaNacimiento" => "Fecha de nacimiento",
            "correo" => "Correo",
            "población" => "Población",
            "mesesTexto" => "Meses de acceso"
        ];
    }

    public function rules(): array
    {
        return [
            [["nombre", "apellidos", "fechaNacimiento", "correo", "poblacion", "mes"], "required"],
            [["fechaNacimiento"], "date", "format" => 'yyyy-MM-dd', "max" => "2023-12-31"],
        ];
    }
    public function getMeses(): array
    {
        return ["Junio" => "Junio", "Julio" => "Julio", "Agosto" => "Agosto", "Septiembre" => "Setiembre"];
    }
    public function getPoblaciones(): array
    {
        return ["Laredo" => "Laredo", "Santander" => "Santander", "Astillero" => "Astillero"];
    }


    public function getMesesTexto()
    {
        return join(", ", $this->mes);
    }
}
