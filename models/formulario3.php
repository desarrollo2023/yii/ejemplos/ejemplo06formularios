<?php

namespace app\models;

use DateTime;
use yii\base\Model;

/**
 * Formulario3
 */
class Formulario3 extends Model
{
    public ?int $dia = null;
    public ?string $mes = "";
    public ?string $poblacion = "";

    public function attributeLabels(): array
    {
        return [
            "dia" => "Día del mes",
            "mes" => "Mes",
            "poblacion" => "Población"
        ];
    }

    public function rules(): array
    {
        $fecha = new DateTime();
        $fecha->modify('last day of this month');
        $dia = $fecha->format('d');
        return [
            [["dia", "mes", "poblacion"], "required"],
            [["dia"], "number", "min" => 1, "max" => $dia]
        ];
    }
    public function meses(): array
    {
        return [
            "Enero" => "Enero",   //lo que graba=>lo que el usuario ve en el desplegable
            "Febrero" => "Febrero",
            "Marzo" => "Marzo",
            "Abril" => "Abril",
            "Mayo" => "Mayo",
            "Junio" => "Junio",
            "Julio" => "Julio",
            "Agosto" => "Agosto",
            "Septiembre" => "Septiembre",
            "Octubre" => "Octubre",
            "Noviembre" => "Noviembre",
            "Diciembre" => "Diciembre",
        ];
    }

    public function poblaciones(): array
    {
        return [
            "Santander" => "Santander", "Torrelavega" => "Torrelavega", "Castro Urdiales" => "Castro Urdiales", "Reinosa" => "Reinosa", "Potes" => "Potes"
        ];
    }
}
