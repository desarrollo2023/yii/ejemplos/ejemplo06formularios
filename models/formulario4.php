<?php

namespace app\models;

use DateTime;
use yii\base\Model;

/**
 * Formulario4
 *  */
class Formulario4 extends Model
{
    public ?string $nombre = null; //cuadro de texto
    public ?int $edad = null; //cuadro de numero
    public ?int $mes = null; // desplegable
    public ?int $categoria = null; //cuadro de lista
    public ?string $dia = null;
    public array $aficiones = []; //multiple
    public array $opciones = []; // multiple
    public ?string $fecha = null; //tipo date

    public function attributeLabels(): array
    {
        return [
            "nombre" => "Nombre completo",
            "edad" => "Edad",
            "mes" => "Mes Uso",
            "categoria" => "Categoría Herramienta",
            "dia" => "Día Uso",
            "aficiones" => "Aficiones",
            "opciones" => "Opciones alquiler",
            "fecha" => "Fecha Alta"
        ];
    }

    public function rules(): array
    {
        return [
            [["nombre", "edad", "mes", "categoria", "dia", "aficiones", "opciones", "fecha"], "safe"],
        ];
    }
    public function getMeses(): array
    {
        return [
            1 => "Enero",
            2 => "Febrero",
            3 => "Marzo",
            4 => "Abril",
            5 => "Mayo",
            6 => "Junio",
            7 => "Julio",
            8 => "Agosto",
            9 => "Septiembre",
            10 => "Octubre",
            11 => "Noviembre",
            12 => "Diciembre",
        ];
    }

    public function getMostraraficiones()
    {
        return ["Lectura", "Deporte", "Tv", "Informática"];
    }
    public function getMostraropciones()
    {
        return ["Día completo", "Con carga", "Limpieza incluida"];
    }
    public function getCategorias()
    {
        return ["A", "B", "C"];
    }
    public function getDias()
    {
        return ["Domingo", "Lunes", "Miércoles", "Viernes"];
    }
    public function getAficionestexto()
    {
        return join(",", $this->aficiones);
    }
    public function getOpcionestexto()
    {
        return join(",", $this->opciones);
    }
}
