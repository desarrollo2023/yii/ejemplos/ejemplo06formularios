<?php

namespace app\models;

use yii\base\Model;

/**
 * Formulario1
 */
class Formulario2 extends Model
{
    public ?int $numero1 = null; // input tipo numero
    public ?int $numero2 = null; // input tipo numero

    public function attributeLabels(): array
    {
        return [
            "numero1" => "Número 1",
            "numero2" => "Número 2",
        ];
    }

    public function rules()
    {
        return [
            [["numero1", "numero2"], "required", 'message' => 'El campo {attribute} es obligatorio'],
            //[["numero1"], "integer", "max" => 100, "min" => 0],
            [["numero1", "numero2"], "integer"],
            [['numero2'], 'compare', 'compareValue' => 0, 'operator' => '!=', 'type' => 'number'],
            [["numero1"], function ($atributo) {
                if (!($this->$atributo > 0 && $this->$atributo < 100)) {
                    return $this->addError($atributo, "El número 1 debe estar entre 0 y 100");
                }
            }]
        ];
    }
    public function suma()
    {
        return $this->numero1 + $this->numero2;
    }
    public function resta()
    {
        return $this->numero1 - $this->numero2;
    }
    public function producto()
    {
        return $this->numero1 * $this->numero2;
    }
    public function cociente()
    {
        return $this->numero1 / $this->numero2;
    }
}
